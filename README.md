**Part 1:**<br />
nodes and hub have a separate processes but they have same properties.<br />
```python
message :{
	'protocol_version':,
	'mesh_id':,
	'sender_id':,
	'send_timestamp':,
	'origin_id': ,
	'origin_timestamp': ,
	'relay_count': ,
	'repeat_count': ,
	'position': #'this is hub' or 'this is node'   ####this property should be determined first....(if we want to Replace the hub of existing project we set this)
	'activity':#'active' or 'inactive' this determine activity of node or hub....first all nodes are active but if a node has a problem or we want to remove it, this property shoud be inactive 
	'routing':#this is a matrix for hub and a list for nodes
	'message_type': { ('initialization':'first' or ....)  or  ('command':'command_1' or 'command_2' or...or 'checking' or 'restart' or 'reset') or  ('notification':'notification_1' or 'notification_2' ,... )   }
	'destination': 'hub' or 'node_1' or  'node_2',...or 'All nodes'
	'rssi_matrix':  # matrix for saving received_rssi
	'sender_list'
}
```
------
**Part 2:**<br />
Time synchronization: nodes should send a message to hub(message['destination'] = 'hub') and send a notification for getting time.<br />
for other properties in  time synchronization protocol, we use notifications.<br />

------
**Part 3:**<br />

------
**Part 4:**<br />
this part should be run first...same as last protocol....hub sends a message for nodes (message['destination'] = 'All nodes')<br />
and every node sends received_message to other nodes...<br />
every node or hub saves the last sender in a list and every node saves received_rssi in rssi_matrix...this will be repeat n time...<br />
after this time,every node with the last sender send messsage.when hub receives a message,save message['rssi_matrix'] in the final_rssi_matrix.<br />
(rssi_matrix and final_rssi_matrix are keys in message )<br />

------
**Part 5:**<br />
message['message_type']['command'] = 'command_1' or 'command_2' or...  this feature was determined in nodes<br />
message['destination'] = node_1' or  'node_2' or node_3' or  'node_4' ,...<br />
```python
command_1 and 2 , 3,....:{
'Changing the state of node upon receive of command':{activity:'yes'or 'no','': or  '': or '': or......},
'Changing the state after a certain time upon receive of command':{activity:'yes'or 'no','time':  ,'mode':OFF or ON,..} ,
'Changing the state on a regular basis': {activity:'yes'or 'no','time1': ,'time2': ,'mode1': ,'mode2': ,...},
'Change the behavior of node': {activity:'yes'or 'no','': , '':,...}
```
**Part 5-1:**<br />
**checking nodes: in this part the hub sends a message and checking all nodes...**<br />
```python
message['message_type']['command']=  'checking'
message['destination'] = 'All nodes'
```
hub sends this message to all nodes and every node that receives this message, do something and sends a new message to hub.<br />
then hub check the received_message and processes it.<br />
something----->>>>>>>testing all commands and check notifications.<br />

**Part 5-2:**<br />
**restart nodes:**<br />
1.Return all features to default(call reset--->(message['message_type']['command']['reset]))<br />
2.create a new message('message_type' = 'initialization')<br />

------
**Part 6:**<br />
message['message_type']['notification'] = notification_1 or notification_2 or.<br />
```python
message['destination'] = 'hub'
notification_1 and 2 , 3,....:{
'Notifying hub of a state change which needs some action to be taken':{activity:'yes'or 'no','previous mode': or  'new mode':  or '': or......},
'Aknowledge receiving or responsing to a command from hub':{activity:'yes'or 'no','value':} ,
'Logging and data collections.': {activity:'yes'or 'no','mode': },
'getting time': {activity:'yes'or 'no',}
}
```




