# ارسال مقدار سنسور با استفاده از پروتکل های صنعتی
 در این پروژه قصد داریم با استفاده از دو پروتکل اینترنت اشیاء صنعتی، اطلاعات یک یا چند سنسور را برای یک نرم افزار ارسال نماییم. پروتکل اول **Modbus TCP** و پروتکل دوم **Opc ua** می باشد.
 در واقع همانطور که در تصویر زیر مشاهده می کنید اطلاعات سنسور از طریق پروتکل  Modbus Tcp از Esp8266 به Raspberry انتقال داده می شود و سپس این اطلاعات دریافت شده در Raspberry از طریق پروتکلOpc به یک نرم افزار برای نمایش (برای مثال things board)ارسال می گردد.
```mermaid
graph LR
A((Ldr sensor))  --> B(Esp8266)
B-- ModbusTcp/Ip --> C(Raspberry pi)
C -- OPC UA--> D((Thingsboard))
```
***
 **Materials Required:**

 - An Esp8266 witty
 - A Raspberry Pi 3 
 
 
Esp8266 witty| Raspberry pi 3 |
:--:|:--:|
|<img src = "https://cdn.instructables.com/F3I/RGHJ/JBMJVA03/F3IRGHJJBMJVA03.LARGE.jpg" width="200"  hight="200" title="esp8266-witty">|<img src = "https://www.raspberrypi.org/app/uploads/2017/05/Raspberry-Pi-3-462x322.jpg" width="200"  hight="200" title="Raspberry pi">|
 - A 4GB or larger microSD card.
 - A micro USB cable.
 - **[Optional]** An ethernet cable.
 - **[Optional]** A [2A micro USB power supply](https://www.raspberrypi.org/products/universal-power-supply/).

***

### Step 1:
 کد مربوط به Modbus Slave در محیط Arduino  و به زبان C نوشته شده است که این کد در ادامه آمده است:

```c

/**
*
*    More information about projects PDAControl
*    Mas informacion sobre proyectos PDAControl
*    Blog PDAControl English   http://pdacontrolenglish.blogspot.com.co/   
*    Blog PDAControl Espa?ol   http://pdacontrol.blogspot.com.co/
*    Channel  Youtube          https://www.youtube.com/c/JhonValenciaPDAcontrol/videos   
*
*/
#include <ESP8266WiFi.h>
#include <ModbusTCPSlave.h>

ModbusTCPSlave Mb;
int ldr = A0;
unsigned long timer;
unsigned long checkRSSIMillis;
void setup()
{    
 //Mb.begin("SSID", "PASSWORD", "IP", "GATEWAY", "SUBNET");
  Mb.begin("Toktam", "toktam1374");
  delay(1000);
  
  Serial.begin(115200);
}

void loop()
{
  
 Mb.Run();
 delay(10);
 //Random Value  1 - 100
 int ldr_value = analogRead(ldr);
 delay(100);
 Mb.MBHoldingRegister[0] = ldr_value;
 //Print Serial Holding Register [0]
 //Serial.println(Mb.MBHoldingRegister[0]);
    
}

 /*****FUNZIONI*****/

byte checkRSSI() {
  byte quality;
  long rssi = WiFi.RSSI();
  if (rssi <= -100)
    quality = 0;
  else if (rssi >= -50)
    quality = 100;
  else
    rssi = rssi + 100;
  quality = byte(rssi * 2);

  return quality;
}
```
همانطور که می بینید برای این بخش از کتابخانه ModbusTCPSlave استفاده شده است.
<br />
**نکته مهم:**
در کد نوشته شده چون دستورanalogRead در یک حلقه به صورت پیوسته اجرا می شود، برنامه دچار مشکل شده و اتصال Esp8266 به wifi قطع می گردد که برای حل آن کافی است یک تاخیر در این حلقه قرار دهیم تا این مشکل حل گردد.
***
### Step 2:
برای چک کردن کد نوشته شده از یک برنامه  شبیه ساز modbus server مانند Modbus Poll استفاده کرده و برای تنظیمات این بخش می توانید از تصویری که در ادامه قرار داده شده است استفاده کنید.هم چنین می توانید این نرم افزار را از لینک [ Modbus Poll Download](http://www.modbustools.com/download.html) دانلود نمایید.
<br />
<br />
<img src = "https://lh3.googleusercontent.com/nlFkt5UMWXRwCN0qLEk01JlxpQeaYgItoCJ5yILSvzIuJembPrz31nSCGCTBAsFAjX3IDSayW6oB" width="300"  hight="300" title="Modbus Poll ">
<br />
<br />
در بخش IP باید IP مربوط به Esp  را وارد کنید
<br />
بعد از تنظیمات اولیه و connect شدن این نرم افزار، مقدار Holding register را مشاهده میکنید که عینا همان مقادیر مربوط به سنسور می باشد.

***
### Step 3:

در این مرحله باید کد مربوط به Modbus serrver را نوشته و برروی Raspberry پیاده سازی نماییم.
کدی که برای این بخش نوشته شده است در ادامه آمده است:
```python
from pyModbusTCP.client import ModbusClient

SERVER_HOST = "192.168.43.129"
SERVER_PORT = 1502
c = ModbusClient()
regs=[]

c.host(SERVER_HOST)
c.port(SERVER_PORT)

while True:
	c.open()
	if not c.is_open():
		if not c.open():
			print("unable to connect to" + SERVER_HOST )
	if c.is_open():
		regs = c.read_holding_registers(0,1)
	if regs:
		print(str(regs))
	c.close()
```
<br />
برای این کد از کتابخانه pyModbusTCP استفاده شده است.IP نوشته شده نیز مربوط به Master Slave می باشد که در اینجا همان Esp8266 است.با اجرای این کد برروی Raspberry  می توانید مقدار سنسور را مشاهده کنید. 
<br />
:smile:

***

### Step 4:
 در ادامه باید از پروتکل OPC UA استفاده کرده و مقدار خوانده شده از سنسور را با استفاده از این پروتکل به یک نرم افزار یا وبسایت مانند Things board ارسال نماییم.
 ما در گام اول سعی می کنیم که با نوشتن یک برنامه برای OPC UA Server بر روی Raspberry مقدار یک شمارنده را برای یک OPC UA Client ارسال کنیم.
 کدی که برای این بخش نوشته شده است در ادامه آمده است :
 
```python
import sys
sys.path.insert(0, "..")
import time
from opcua import ua, Server


if __name__ == "__main__":
    

    # setup our server
    server = Server()
    server.set_endpoint("opc.tcp://192.168.43.15:4840")

    # setup our own namespace, not really necessary but should as spec
    uri = "http://examples.freeopcua.github.io"
    idx = server.register_namespace(uri)

    # get Objects node, this is where we should put our nodes
    objects = server.get_objects_node()

    # populating our address space
    myobj = objects.add_object(idx, "MyObject")
    myvar = myobj.add_variable(idx, "MyVariable",11)
    myvar.set_writable()    # Set MyVariable to be writable by clients

    # starting!
    server.start()
    
    try:
        count = 0
        while True:
            time.sleep(1)
            count += 0.1
            myvar.set_value(count)
    finally:
        #close connection, remove subcsriptions, etc
        server.stop()
```
سپس با استفاده از نرم افزار UaExpert می توانید مقدار متغیر count را مشاهده نمایید.
برای تنظیمات مربوط به Raspberry و هم چنین کار با این نرم افزار ویدئوی زیر را مشاهده کنید:

[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/YOUTUBE_VIDEO_ID_HERE/0.jpg)](https://www.youtube.com/watch?v=5V9JELPc0Ck&t=255s)

---
### Step 5:
حال لازم است تا کد هایی که برای Modbus Master و OPC UA Server نوشته شده است را با هم ادغام کرده و هم چنین مقدار رجیسترHoldingRegister که حاوی مقدار سنسور است را از طریق پروتکل OPC UA برای نرم افزار UaExpert ارسال نماییم.
کد نهایی که بر روی Raspberry pi  قرار می گیرد در ادامه آمده است:

```python
from pyModbusTCP.client import ModbusClient
import sys
sys.path.insert(0, "..")
import time
from opcua import ua, Server



if __name__ == "__main__":
    
	
    SERVER_HOST = "192.168.43.129"
    SERVER_PORT = 1502
    c = ModbusClient()
    regs=[]

    c.host(SERVER_HOST)
    c.port(SERVER_PORT)

    # setup our server
    server = Server()
    server.set_endpoint("opc.tcp://192.168.43.15:4840")

    # setup our own namespace, not really necessary but should as spec
    uri = "http://examples.freeopcua.github.io"
    idx = server.register_namespace(uri)

    # get Objects node, this is where we should put our nodes
    objects = server.get_objects_node()

    # populating our address space
    myobj = objects.add_object(idx, "MyObject")
    myvar = myobj.add_variable(idx, "MyVariable",11)
    myvar.set_writable()    # Set MyVariable to be writable by clients

    # starting!
    server.start()
    
    try:
        count = 0
        while True:
            c.open()
            if not c.is_open():
                if not c.open():
                    print("unable to connect to" + SERVER_HOST )
            if c.is_open():
                regs = c.read_holding_registers(0,1)
            if regs:
                time.sleep(1)
                count = regs[0]
                myvar.set_value(count)
            c.close()
            
    finally:
        #close connection, remove subcsriptions, etc
        server.stop()
```


با اجرای این کد بر روی Raspberry و اجرای نرم افزار UaExpert می توانید مقدار سنسور را مشاهده نمایید.


 
 

